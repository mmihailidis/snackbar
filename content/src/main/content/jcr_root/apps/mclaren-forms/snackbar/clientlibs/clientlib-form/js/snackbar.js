function showSnackbar(pShowText) {
  // Get the snackbar DIV
  var x = document.getElementById("snackbar");

  x.innerHTML = pShowText;

  // Add the "show" class to DIV
  x.className = "show";

  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 20000);
}

function showSnackbarT(pData) {
    var langid = getRequestParameterByName(window.location.href, 'afAcceptLang');
    if (langid === null) {
        langid = getRequestParameterByName(window.document.referrer, 'afAcceptLang');
    }
    if (langid !== null) {
        var basename = guideBridge.resolveNode("guide[0].guide1[0]").jcrPath + "/assets/dictionary";
        var language = langid;
        var strData = "[" + pData + "]";
        $.ajax({
            type: "GET",
            url: "/bin/mclaren/getI18nText",
            data: {
                basename: basename,
                language: language,
                text: strData
            },
            success: function (res) {
                if (res !== null) {
                  	showSnackbar(res);
                  }
            }

        });

    } else {
        showSnackbar(pData);
	}
}

/* COMMON FUNCTIONS */

// Function to get a URL parameter by name
// Example of link: window.location.href window.document.referrer
function getRequestParameterByName(link, name) {
  	var paramValue = null;
  	if ((link !== null) && (link.length > 0)) {
      var url = new URL(link);
      paramValue = url.searchParams.get(name);
    }
	return paramValue;
}
